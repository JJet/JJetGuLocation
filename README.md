# JJetGuLocation
This is a demo imitated the MeiTuan(iPhone version) APP’s location capability in its Main page。仿照美团iPhone版主页定位实现，可以用在实际项目开发中！

# 实现效果如下：
![image](https://github.com/JJetGu/JJetGuLocation/raw/master/screenShot/QQ20150421-1@2x.png)
![image](https://github.com/JJetGu/JJetGuLocation/raw/master/screenShot/QQ20150421-1@2x%E5%89%AF%E6%9C%AC%202.png)
![image](https://github.com/JJetGu/JJetGuLocation/raw/master/screenShot/QQ20150421-1@2x%E5%89%AF%E6%9C%AC.png)

## 宝贵意见
如果有任何问题，请到Git@OSC中发消息给我，写上你不明白的地方，我会在Git@OSC给予帮助。[这个是我的GitHub地址](https://github.com/JJetGu)